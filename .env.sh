#!/bin/sh

# shell expands only unquoted tilde
mkdir -p ~/.local/bin
export PATH="${HOME}/.local/bin:${PATH}"
export MANPATH="${HOME}/.local/share/man:${MANPATH}"

# export LD_LIBRARY_PATH=~/.local/lib
if [ -n "${WSL_DISTRO_NAME}" ] ; then  # or grep /proc/version
    # under wsl, when going for micro editor, tab completion will suggest every
    # microsoft-* crappy exe, as windows folders are in $PATH... We remove them,
    # and add a our windows dir, containing a clipboard integration script.
    PATH="$(printf '%s' "${PATH}" | awk -v RS=: -v ORS=: '/mnt/ {next} {print}')"
    PATH="${HOME}/.local/wsl:${PATH}"

    apport_comp=/etc/bash_completion.d/apport_completion
    if [ -e "${apport_comp}" ] ; then
        printf "'apport' autocompletion warnings: rename %s\n" "${apport_comp}"
    fi
    unset apport_comp
fi

mount | grep -q " /tmp type tmpfs" || printf "WARNING: /tmp is not tmpfs.\n"
# Add temporary files according to the user.
# export TMPDIR="/tmp/$USER"
# if [ ! -d "$TMPDIR" ]; then
#     mkdir -m 700 "$TMPDIR"
# fi
export TMPDIR=/tmp
# export LANG=en_US.UTF-8

# '$HOME' and '$SHELL' env vars are set at login time
# '$TERM' is set by terminal. But on Alpine, the ssh client-server '$TERM' negociation
# somehow falls back to an uneffective 'screen' value.
# Can not 'infocmp | ssh root@remote-host "tic -"' as 'tic' is not on Alpine Standard
# So we force 'TERM', so it is set on server side when ssh-ing
# export TERM=xterm
# https://superuser.com/questions/271925
# https://specifications.freedesktop.org/basedir-spec/latest/ar01s03.html
export XDG_CONFIG_HOME=~/.config

set -a  # posix, export all vars until 'set +a'
. "${XDG_CONFIG_HOME}/user-dirs.dirs"  # "source" is not posix, but "." is
set +a

export BROWSER=firefox  # script has precedence in $PATH
# polkit-gnome (gparted) throws an error if COLORTERM is a full path
export TERMINAL=foot
export COLORTERM=truecolor
export COLOREXE=$TERMINAL
# $COLORTERM -c

if command -v micro >/dev/null 2>&1 ; then
    export EDITOR=micro
elif command -v nano >/dev/null 2>&1 ; then
    export EDITOR=nano
elif command -v vim >/dev/null 2>&1 ; then
    export EDITOR=vim
else
    export EDITOR=vi
fi
# "visual" as in "requires advanced terminal features", not like "geany".
export VISUAL="${EDITOR}"

export PAGER=less
# avoid search history file's creation (less man page)
export LESSHISTFILE=-
export LESS='-RMI --mouse --wheel-lines=3 --shift 5'
# mouse option prevent any mouse text selection
# export LESS='-RMFI --shift 5 --mouse --wheel-lines=3'

# MANPATH=$MANPATH:...
# MANPAGER=manpager

# https://lists.gnu.org/archive/html/bug-wget/2017-03/msg00051.html
export WGETRC=~/.config/wgetrc

# dircolors is part of coreutils package, not installed on alpine standard.
if [ -z "${LS_COLORS}" ] && command -v dircolors >/dev/null ; then
    # gentoo runs dircolors from /etc/bash/bashrc, but ubuntu not.
    eval "$(dircolors -b ~/.dir_colors)" ;
fi

# python caches only modules anyway
export PYTHONDONTWRITEBYTECODE=1
export PYTHONSTARTUP=~/.config/pythonrc

