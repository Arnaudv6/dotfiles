#!/bin/sh

set -b  # asynchronous returns for background jobs

# make the (colored) prompt (can't be in .env.sh)
# ash understands most-all of bash prompt escape sequences
# failsafe (colorless) PS1: export PS1='\u@\h \w \$ '
if [ "$(id -u)" = 0 ]; then  # root UID
    export PS1="\[\033[01;31m\]\h\[\033[01;34m\] \w \\$\[\033[00m\] "
else
    export PS1="\[\033[01;32m\]\u@\h\[\033[01;34m\] \w \\$\[\033[00m\] "
fi

# Embed term's title in $PS1 to restore it in case a command changed it.
# no need to check TTY/SSH: it seems setting terminal title creates no issue.
# '\[' and '\]' tell bash not to account those chars in prompt'length
PS1="${PS1}\[\e]2;\s\\$ \W\a\]"

# env vars that work equally with bash and ash
HISTSIZE=30  # number of line to remember

# Can't simplify to 'case "${SHELL}"', as $SHELL is /bin/sh for POSIX needs see .README.
# shellcheck disable=SC3044
if [ -n "$BASH_VERSION" ]; then
    unset HISTFILE
    # though we don't save history on disk, we still want a nice experience while shell lives
    HISTCONTROL="ignorespace:erasedups"
    # HISTCONTROL=ignoredups
    # Don't save lines which begin with a <space> character and eliminate duplication.
    # HISTIGNORE_BASE="[bf]g:cd:cd ~:cd ..:..:date:exit:halt:* --help:* help:history"

    # use bash more complete autocompletion
    shopt -s progcomp
    shopt -s no_empty_cmd_completion
    . ~/.local/share/bash-completion/apk
    if [ -r /usr/share/bash-completion/bash_completion ]; then
        # shellcheck disable=SC1091
        . /usr/share/bash-completion/bash_completion
    fi
    complete -cf doas
    complete -cf dark
    complete -cf nh

    PROMPT_DIRTRIM=2
elif [ "${SHELL}" = "/bin/ash" ] ; then
    # (in ash, to reduce PS1, change '\w' for '\W')
    # busybox ash saves history after each command: security concern and wears HDD
    command ln -s -f /dev/null .ash_history
fi

# aliases.
# Double quotes are OK: commands are not expanded as variables would be.
# Beware pipes in aliases: can't add options to 1st command from CLI. Prefer functions.
alias su="su -"                 # login-like exp, preserve neither aliases nor end.
# '\su' is in-between: keeps aliases but no env. Usual but not-so-sensible exp. IMO.
# '-p' preserves environment (.inputrc, $TERM…). NOK kernel builds…
alias mkdir="mkdir -pv"         # -v is not posix
alias ls="ls -alhF --color"     # -h and --color (short for always) are not posix
alias grep="grep --color=auto"  # GNU only, no (bad) effect in BusyBox
alias cp="cp -iv"               # prompt before overwrite, -v is not posix
alias mv="mv -iv"               # prompt before overwrite, -v is not posix
alias ln="ln -v"                # -v is not posix, '-i' does not stand for interractive

# Beware bad habbits when aliasing rm to have securing confirmations.
# '-I' is not posix and does not warn for 1-3 removals.
alias rm="rm -iv"               # '-i': prompt for each file (override with '-f')

# less is configured in $ENV so 'man' and 'delta' benefit too.
alias ness="less -N"
# awk '{print NR " " $s}' will add line numbers to any command. 'nl -b a' is not posix.
alias ps_nice="ps -A -o pid,s,user,pcpu,vsz=MEM,args"
alias diff="git diff --no-index --"
alias vim="vim -i NONE"         # do not create viminfo file
alias xo="xdg-open"
alias calc="printf '\e]0;calc\a' ; bc -l"
alias hexdump="hexdump -C"
alias cyanrip="cyanrip -N -D /tmp/rip"
alias films="lizzy --profile=films"
alias eightyChars="printf '\e[78C=><=\n'"

# simple, (cascade-)pipeable snippets, prefer 'sed' over 'tr' to accept file input
alias strip="sed 's/^[ \t]*//;s/[ \t]*$//'"
alias stripl="sed 's/^[ \t]*//g'"
alias stripr="sed 's/[ \t]*$//g'"
alias comment_non_empty="sed 's/^./# &/'"
alias comment_all="sed -e 's/^/# /'"
alias single_quote_it="sed \"s/.*/'&'/\""
alias double_quote_it="sed 's/.*/\"&\"/'"

# beware: '#' might sometimes be part of an expression, not a comment
# this should be an awk script, to remove all commented lines, and only remove commented part of normal lines
# https://unix.stackexchange.com/a/608110
alias drop_comments="sed '/^[[:blank:]]*#/d;s/[[:blank:]]*#.*//'"
alias drop_empty_lines="grep ."  # grep -v '^$'
# tr -d '\n' don't take a file input, sed -z 's/\n//g' is not posix.
alias drop_newlines="paste -s -d '\0'"
alias drop_colors="sed 's/\x1B\[[0-9;]\{1,\}[A-Za-z]//g'"  # sed 's/\x1b\[[0-9;]*m//g' -
alias show_long_lines="grep -E -n '.{89}'"
alias show_special_chars="sed -n 'l'"  # | grep '\\\\[0-9]*'
alias show_non_ascii="grep -n -P '[\x80-\xFF]'"  # grep -P is not posix
# alias show_non_ascii="awk '/[^[:ascii:]]/ { print }'"  # [:ascii:] is not posix

# email regexes get more complicated:
# https://www.oreilly.com/library/view/regular-expressions-cookbook/9781449327453/ch04s01.html
alias show_emails="grep -Eo '[[:alnum:]\.\_\%\+\-]+@[[:alnum:]\.\-]+[[:alpha:]]{2,6}+'"
alias nl_from_mac_cr="sed 's/\r/\n/'"
alias nl_from_dos_crnl="sed 's/\r//'"
alias nl2space="paste -s -d ' '"  # not sed 's/\n/ /', see https://stackoverflow.com/a/7697604
# shellcheck disable=SC2154 # $a is never assigned
alias ensure_end_newline="sed -e '\$a\\'"

# system specific temp, more detailed than 'cat /sys/class/thermal/thermal_zone0/temp'
alias temperature="cat /sys/devices/platform/coretemp.0/hwmon/hwmon*/temp*input"
alias frequencies="cat /proc/cpuinfo | grep MHz"
# alias disk_usage="du -d 1 -h | sort -hr | grep -v ^0"

alias recent_music='ls -ltA "${XDG_MUSIC_DIR}/MP3" | head -n 20'
alias recent_clips='ls -ltA "${XDG_MUSIC_DIR}/Inclassés/Videos/" | head -n 20'
alias m3u_from_pwd='find "$PWD" -type f | sort > "${PWD}/${PWD##*/}.m3u"'

alias wget_no_cert="printf '\e]0;wget\a' ; wget --no-check-certificate"
alias rtorrent="printf '\e]0;rtorrent\a' ; rtorrent"
alias public_ip4="curl -w '\n' https://ipinfo.io"
alias local_ip="ip -br addr show"
alias weather="curl -w '\n\n' -4 http://wttr.in/Lyon"

# pylint, bandit and flake8 are non-destructive with right config
alias black="black --diff --color -S"
# alias pydoc2='python -c "help()"'
alias json_pretty="python3 -m json.tool"

# posix, secure solution inspired from https://unix.stackexchange.com/questions/230673
#alias mkpassword="openssl rand -base64 15 | tr '+/' _-"  # tr: base64 -> base64url
alias mkpassword="tr -dc A-Za-z0-9_ </dev/urandom | fold -w 18 | grep _ | head -n1"
alias mkpincode8="tr -dc 0-9 </dev/urandom | fold -w 8 | head -n1"

# create such an alias? BEWARE white-spaces, equal sign... conv=fsync?
# alias flashusb=dd bs=4M if=/image.iso of=/dev/sdc status=progress oflag=sync

# find does not properly close pipes, tail does
f () { printf -- '-ipath "*%b*" ' "$@" | xargs find ./ | tail -n +1 ; }
# grep -r is not POSIX but OK in busybox, and its output is much cleaner than:
# find ./ -type f -exec grep -qi "$1" {} \; -print
ft () { grep -ri -- "$1" * ; }
# alias ftgit="git grep"

# execute command in argument, for each line in stdin (piped-in).
# tail will close pipe properly, in case 'find' does not, the rest is escaping.
xa () { tail -n +1 | sed "s/'/\\\\'/" | sed "s/\"/\\\\\"/" | xargs -I{} "$@" {} ; }

yta () { mpv --ytdl-format=bestaudio ytdl://ytsearch:"$*" ; }
dark () { env GTK_THEME=Arc-Darker:dark "$@" ; }  # see 'complete -cf dark' above

please () {
    # "sudo !!" is not expanded when called from either an alias or a function.
    # fc is defined by POSIX but not implemented in busybox, and ash knows not '!!'
    if command -v doas >/dev/null 2>&1 ; then
        # shellcheck disable=SC2046
        doas $(fc -ln -1)
    elif command -v sudo >/dev/null 2>&1 ; then
        # shellcheck disable=SC2046
        sudo $(fc -ln -1)
    else
        printf "Install doas or sudo\n" >&2
        exit 1
    fi
}

nh () {
    if ! command -v "$1" >/dev/null 2>&1 ; then
        echo >&2 "Could not find $1 executable."
        return 1
    fi
    nohup "$@" </dev/null >/dev/null 2>&1 &
}

# alias wget="wget -c" # no need: set in $WGETRC conf file
wget_page_to_pwd () {
    wget --mirror --page-requisites --adjust-extension --no-parent --convert-links\
        -erobots=off --domains "$(echo "$1" | sed -e 's|^[^/]*//||' -e 's|/.*$||')" "$1"
    # sed finds domain from url
}
