-- put user settings here
-- this module will be loaded after everything else when the application starts
-- it will be automatically reloaded when saved

local core = require "core"
local keymap = require "core.keymap"
local config = require "core.config"
local style = require "core.style"
local common = require "core.common"

------------------------------ Themes ----------------------------------------

-- light theme:
-- core.reload_module("colors.summer")
-- core.reload_module("colors.default-arnaud")
-- /usr/share/lite-xl/colors/default.lua
style.background = { common.color "#1e1e22" }
style.line_highlight = { common.color "#252529" }


--------------------------- Key bindings -------------------------------------

keymap.add { ["ctrl+q"] = "core:quit" }
-- pass 'true' for second parameter to overwrite an existing binding
keymap.add({ ["ctrl+e"] = "core:find-command" }, true)
keymap.add({ ["ctrl+alt+left"] = "root:switch-to-previous-tab" }, true)
keymap.add({ ["ctrl+alt+right"] = "root:switch-to-next-tab" }, true)
keymap.add({ ["ctrl+alt+shift+left"] = "root:move-tab-left" }, true)
keymap.add({ ["ctrl+alt+shift+right"] = "root:move-tab-right" }, true)
keymap.add({ ["ctrl+d"] = "doc:duplicate-lines" }, true)
keymap.add({ ["ctrl+r"] = "core:restart" }, true)
keymap.add({ ["ctrl+down"] = "doc:create-cursor-next-line" }, true)
keymap.add({ ["ctrl+up"] = "doc:create-cursor-previous-line" }, true)
keymap.add({ ["alt+left"] = "doc:move-to-start-of-line" }, true)
keymap.add({ ["alt+right"] = "doc:move-to-end-of-line" }, true)
keymap.add({ ["alt+shift+left"] = "doc:select-to-start-of-line" }, true)
keymap.add({ ["alt+shift+right"] = "doc:select-to-end-of-line" }, true)
keymap.add({ ["home"] = "doc:move-to-start-of-doc" }, true)
keymap.add({ ["end"] = "doc:move-to-end-of-doc" }, true)
keymap.add({ ["shift+home"] = "doc:select-to-start-of-doc" }, true)
keymap.add({ ["shift+end"] = "doc:select-to-end-of-doc" }, true)
keymap.add({ ["ctrl+t"] = "core:new-doc" }, true)

-- those are overwritten in search-ui
keymap.add({ ["ctrl+n"] = "find-replace:repeat-find" }, true)
keymap.add({ ["ctrl+p"] = "find-replace:previous-find" }, true)
-- I don't use it, but blank page refers to it...
keymap.add({ ["ctrl+shift+f"] = "core:find-file" }, true)



------------------------------- Fonts ----------------------------------------

-- customize fonts:
-- style.font = renderer.font.load(DATADIR .. "/fonts/FiraSans-Regular.ttf", 14 * SCALE)
-- style.code_font = renderer.font.load(DATADIR .. "/fonts/JetBrainsMono-Regular.ttf", 14 * SCALE)
style.code_font = renderer.font.load("/usr/share/fonts/hack/Hack-Regular.ttf", 16 * SCALE)
style.selectionhighlight = { common.color "#FFFFFF" }
-- DATADIR is the location of the installed Lite XL Lua code, default color
-- schemes and fonts.
-- USERDIR is the location of the Lite XL configuration directory.
--
-- font names used by lite:
-- style.font          : user interface
-- style.big_font      : big text in welcome screen
-- style.icon_font     : icons
-- style.icon_big_font : toolbar icons
-- style.code_font     : code
--
-- the function to load the font accept a 3rd optional argument like:
--
-- {antialiasing="grayscale", hinting="full", bold=true, italic=true, underline=true, smoothing=true, strikethrough=true}
--
-- possible values are:
-- antialiasing: grayscale, subpixel
-- hinting: none, slight, full
-- bold: true, false
-- italic: true, false
-- underline: true, false
-- smoothing: true, false
-- strikethrough: true, false

------------------------------ Plugins ----------------------------------------

-- enable or disable plugin loading setting config entries:

-- enable plugins.trimwhitespace, otherwise it is disabled by default:

-- disable detectindent, otherwise it is enabled by default
-- config.plugins.detectindent = false
config.indent_size = 4

config.plugins.colorpicker = true
config.plugins.colorpreview = true
config.plugins.trimwhitespace = true

config.disabled_transitions = {
  scroll = true
}

config.plugins.drawwhitespace = {
  trailing_color = { common.color "#FF2222" },
  -- trailing_color = style.error,
  substitutions = {
    {
      char = " ",
      sub = "█",
      show_leading = false,
      show_middle = false,
      show_trailing = true,
    },
    {
      char = "\t",
      sub = "·",
    },
  }
}
config.plugins.drawwhitespace.enabled = true

config.plugins.lineguide = common.merge({
  enabled = true,
  width = 2,
  rulers = {
    0, 88,
  }})


local lspconfig = require "plugins.lsp.config"
lspconfig.gopls.setup {
  name = "gopls",
  language = "go",
  file_patterns = { "%.go$" },
  command = { "gopls", "serve" },
  verbose = false,
  settings = {
    gopls = {
      ["formatting.gofumpt"] = true,
      ["staticcheck"] = true,
      ["ui.verboseOutput"] = true,
    },
  },
}

config.plugins.lsp.mouse_hover = true
config.plugins.lsp.mouse_hover_delay = 300
config.plugins.lsp.show_diagnostics = true
config.plugins.lsp.stop_unneeded_servers = true
config.plugins.lsp.log_file = ""
config.plugins.lsp.prettify_json = false
config.plugins.lsp.log_server_stderr = false
config.plugins.lsp.force_verbosity_off = false
config.plugins.lsp.more_yielding = false

---------------------------- Miscellaneous -------------------------------------

-- modify list of files to ignore when indexing the project:
-- config.ignore_files = {
--   -- folders
--   "^%.svn/",        "^%.git/",   "^%.hg/",        "^CVS/", "^%.Trash/", "^%.Trash%-.*/",
--   "^node_modules/", "^%.cache/", "^__pycache__/",
--   -- files
--   "%.pyc$",         "%.pyo$",       "%.exe$",        "%.dll$",   "%.obj$", "%.o$",
--   "%.a$",           "%.lib$",       "%.so$",         "%.dylib$", "%.ncb$", "%.sdf$",
--   "%.suo$",         "%.pdb$",       "%.idb$",        "%.class$", "%.psd$", "%.db$",
--   "^desktop%.ini$", "^%.DS_Store$", "^%.directory$",
-- }

