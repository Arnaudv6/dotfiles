# Oxygen Icons

Oxygen icon theme

Modified by Arnaudv6
-> removed unused resolutions 256x256, 128x128, 16x16, 8x8...
   as I only use 22x22 in libreoffice and 48x48 in thunar / waypiedock
-> symlinked video/music/photos folders to blue counter-parts, for consistency

## Introduction

Oxygen-icons is a freedesktop.org compatible icon theme.
