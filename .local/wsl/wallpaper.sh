#!/bin/sh

src_win="$(wslvar USERPROFILE)\\\Desktop\\\expleo\\\wallpaper_expleo.jpg"
src_unix="$(wslpath "${src_win}" | sed 's/\r//')"
dst1="$(wslpath "$(wslvar APPDATA)/Expleo/" | sed 's/\r//')"

for interval in 15 14 13 12 11 10 9 8 7 6 5 4 3 2 1 ; do
	printf "Attempts to go: %s.\n" "${interval}"
    /mnt/c/Windows/System32/reg.exe \
        add 'HKEY_CURRENT_USER\Control Panel\Desktop' \
        /v Wallpaper /t REG_SZ /d "$src_win" /f
    sleep 1
    cp -fv "${src_unix}" "${dst1}"
    sleep 1
    /mnt/c/Windows/System32/rundll32.exe \
        user32.dll, UpdatePerUserSystemParameters 1, True
    # /mnt/c/Windows/System32/rundll32.exe user32.dll, UpdatePerUserSystemParameters
    # DllCall("shell32.dll", "none", "SHChangeNotify", "long", 0x8000000, "uint", BitOR(0x0, 0x1000), "ptr", 0, "ptr", 0)
    sleep 1

    /mnt/c/Windows/System32/rundll32.exe \
        user32.dll, SystemParametersInfo 20, 0, "$src_win", 3
    # 20 (0x0014) stands for SPI_SETDESKWALLPAPER:
    # https://learn.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-systemparametersinfoa
    # https://www.purebasic.fr/english/viewtopic.php?p=543720&sid=4ce79fa0c053ac0fffd2032b158d0e7f#p543720

    sleep 4  # loop and sleep. speak slowly. this is Windows.
done
