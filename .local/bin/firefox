#!/bin/sh
# shellcheck disable=SC2317

# default paths:
#   profiles:   "$HOME/.mozilla/firefox"
#   cache:      "$HOME/.cache/mozilla"

# asumption: one, single profile

FF_in_RAM=/tmp/FFinRAM
arch_name=firefox_profile.tar.gz
arch_path="${HOME}/Sauvegardes/${arch_name}"
ff_binary="$(command -pv firefox)"
REUSE_NOTIF=

IN_TERMINAL=
# memoizing, because can't 'test' this from a (pipe) subshell.
if [ -t 0 ] ; then IN_TERMINAL=YES ; fi

alert () {
    if [ -t 0 ] ; then
        printf "%s\n" "$1"
    else
        notify-send -i firefox "${0##*/}" "$1"
    fi
}

progress_notification() {
    # usage: progress_notification <TEXT> <PROGRESS_PERCENTAGE>
    if [ "${IN_TERMINAL}" = YES ] ; then
        printf "%s %d%%\n" "$1" "$2"
    else
        if [ -z "${REUSE_NOTIF}" ] ; then
            REUSE_NOTIF="$(notify-send -i firefox "${0##*/}" "$1" -p -h int:value:"$2")"
        else
            REUSE_NOTIF="$(notify-send -i firefox "${0##*/}" "$1" -p \
                -r "${REUSE_NOTIF}" -h int:value:"$2")"
        fi
    fi
}

progress_notif_lines() {
    if [ "$#" -ne 1 ] ; then die "kill_usr1_loop() was not passed 1 argument."; fi
    # can't xargs: progress_notification() is a function (!= utility)
    while read -r line ; do
        # percentage seems to never evaluates to 0 near the end with bc.
        # if [ "${line}" -ne 0 ] ; then
        progress_notification "${1}" "${line}"
    done
}

die () {
    if [ -t 0 ] ; then
        printf "%s\n" "$1" >&2
    else
        notify-send -t 0 \
            -i ~/.local/share/icons/oxygen-icons/48x48/status/dialog-error.png \
            "${0##*/}" "$1"
    fi
    exit 1
}

check_system_dependency() {
    # note: command '-p' flag will test dep against system's path
    if ! command -vp "$1" >/dev/null 2>&1 ; then
        die "'${1}' not in path: missing package?"
    fi
}

check_system_dependency notify-send # sensible: won't run firefox headless
check_system_dependency tar
check_system_dependency readlink # busybox / GNU-coreutils
check_system_dependency nproc # busybox / GNU-coreutils
check_system_dependency sqlite3

# Avoid dying with launcher bar/pie (on SIGHUP, waybar sends SIGTERM to children).
trap -- '' HUP TERM QUIT ABRT # 'trap -' would only reset to default.
trap 'die "received SIGINT"' INT
# not trapping SIGEXIT: still handle ^C in parent terminal.
# disown is not POSIX.
# closing std{in|out|err} is not needed, even when running from terminal
# 'exec 0<&- ; exec 1>&- ; exec 2>&-' # 'man exec'

kill_usr1_loop() {
    # usage: 'kill_usr1_loop PID'. Will Send USR1 signal to PID until it is done.
    if [ "$#" -ne 1 ] ; then die "kill_usr1_loop() was not passed 1 argument."; fi
    while kill -s USR1 "$1" >/dev/null 2>&1 ; do
        # POSIX sleep only accepts an integer, can't do a fraction of a second
        # https://stackoverflow.com/questions/7757814/
        # could use 'read'...
        sleep .5
    done
}

unbuffered_sed(){
    while read -r line ; do
        printf "%s\n" "${line}" \
        | sed -n "s/\([a-zA-Z: ]*\)\([0-9]*\)\(.*\)/\2*100\/${1}/p"
    done
}

setup_dirs() {
    symlink_to_tmp () {
        if [ ! -L "$1" ] ; then
            rm -rf "$1"
            ln -s "${FF_in_RAM}" "$1" || die "ln failed."
        fi
    }

    mkdir -p "${FF_in_RAM}" || die "mkdir failed."
    chmod 700 "${FF_in_RAM}"
    symlink_to_tmp "$HOME/.mozilla"
    symlink_to_tmp "$HOME/.cache/mozilla"

    # extract last saved profile at need
    if [ ! -d "${FF_in_RAM}/firefox" ] && [ -f "${arch_path}" ] ; then
        progress_notification 'Extracting profile…' 0

        size_bytes=$(( $(du -sk "${arch_path}" | awk '{print $1}') * 1000 ))
        tar -C "${FF_in_RAM}" -xzf "${arch_path}" --totals=SIGUSR1 2>&1 \
        | unbuffered_sed "${size_bytes}" \
        | bc \
        | progress_notif_lines "Extracting profile…" &

        PID="$(
            env LC_MESSAGES=POSIX ps -o pid=,ppid=,cmd= \
            | grep -e "${$} *tar" \
            | sed "s/${$}.*//g" \
            | tr -d ' '
        )"
        kill_usr1_loop "${PID}"
    fi
}

setup_dirs

retreive_profile_dir() {
    PROFILE_DIR="$(grep 'Path=' "${FF_in_RAM}"/firefox/profiles.ini | cut -d'=' -f2)"
    if [ -z "${PROFILE_DIR}" ] || [ ! -d "${FF_in_RAM}/firefox/${PROFILE_DIR}" ] ; then
        die "Expected exactly 1 profile in firefox/profiles.ini"
    fi
    PROFILE_DIR="${FF_in_RAM}/firefox/${PROFILE_DIR}"
    export PROFILE_DIR
}

retreive_profile_dir

running_instance() {
    # https://bugzilla.mozilla.org/show_bug.cgi?id=151188#c108
    # ./firefox/<profile_name>/lock is a symlink to a pid
    # this cut won't work if ip in symlink is ipv6
    pid="$(readlink "${PROFILE_DIR}"/lock 2>&1 | cut -s -d ':' -f 2)"
    env LC_MESSAGES=POSIX ps -p "${pid}" -o cmd= | grep -q 'firefox'
}

actions_on_instance() {
    if running_instance ; then
        # raise window in WM/compositor
        wlrctl toplevel focus -id firefox
        if [ "$#" -eq 0 ] ; then
            exit
        else
            "${ff_binary}" --new-tab "${1}" || die "Child firefox bin finished improperly."
            # '--new-tab': for redirector extension to work
            exit
        fi
    fi
}

actions_on_instance "$@"

NL='
'
PIDs_SS_STRING=
export PIDs_SS_STRING  # space-separated list of PIDs, as a string.
track_pids(){
    # Usage: no argument => returns list size. PID as argument => put in list.
    case "$#" in
        0)
            PIDs_SS_STRING="$( (
                printf '%s' "${PIDs_SS_STRING}"
                env LC_MESSAGES=POSIX ps -eo pid= | tr -d ' '
            ) | sort | uniq -d ; )"  # drop all dead PIDs from the list.
            printf "%s" "${PIDs_SS_STRING}" | wc -l
            # printf "string: %s\n" "${PIDs_SS_STRING}" >&2
            ;;
        1)
            PIDs_SS_STRING="${PIDs_SS_STRING}${1}${NL}"
            ;;
        *)
            die "Calling track_pids() with too many arguments."
    esac
}

# possible direction: automate firefox?
# $(command -vp firefox) --help
#can't use a DNS proxy as SSL would not agree...
#put extensions and their parameters in VCS?
#wget \
# https://addons.mozilla.org/firefox/downloads/latest/1865/addon-1865-latest.xpi \
# https://addons.mozilla.org/firefox/downloads/latest/433/addon-433-latest.xpi \
# https://addons.mozilla.org/firefox/downloads/latest/3006/addon-3006-latest.xpi &&
#firefox *.xpi && rm *.xpi

#https://git.sr.ht/~chrisppy/dotfiles/tree/main/item/.local/bin/urlfilter

"${ff_binary}" "$@" || die "Child firefox bin finished improperly."
# todo: is 'firefox --backgroundtask' seldom running after firefox dies?

progress_notification 'Archiving profile…' 0

# https://support.mozilla.org/en-US/kb/recovering-important-data-from-an-old-profile
# https://support.mozilla.org/en-US/kb/profiles-where-firefox-stores-user-data

if [ -z "${PROFILE_DIR}" ] ; then die 'null variable: PROFILE_DIR' ; fi

# FOLDERS. '-f': don't complain if non-existent.
rm -rf "${FF_in_RAM}/firefox/Crash Reports"
rm -rf "${FF_in_RAM}/firefox/Pending Pings"
rm -rf "${PROFILE_DIR}/cache2"
rm -rf "${PROFILE_DIR}/crashes"
rm -rf "${PROFILE_DIR}/datareporting"
rm -rf "${PROFILE_DIR}/gmp"
rm -rf "${PROFILE_DIR}/gmp-gmpopenh264"
rm -rf "${PROFILE_DIR}/gmp-widevinecdm"
rm -rf "${PROFILE_DIR}/minidumps"
rm -rf "${PROFILE_DIR}/OfflineCache"
rm -rf "${PROFILE_DIR}/safebrowsing"
rm -rf "${PROFILE_DIR}/sessionstore-backups"
rm -rf "${PROFILE_DIR}/startupCache"
rm -rf "${PROFILE_DIR}/storage/permanent"
rm -rf "${PROFILE_DIR}/storage/to-be-removed"

# FILES. '-f': don't complain if non-existent.
rm -f "${PROFILE_DIR}/cert8.db"
rm -f "${PROFILE_DIR}/cert9.db"
rm -f "${PROFILE_DIR}/chromeappsstore.sqlite"
rm -f "${PROFILE_DIR}/cookies.sqlite"
rm -f "${PROFILE_DIR}/formhistory.sqlite"
rm -f "${PROFILE_DIR}/key4.db"
rm -f "${PROFILE_DIR}/logins.json"
rm -f "${PROFILE_DIR}/places.sqlite-shm"
rm -f "${PROFILE_DIR}/places.sqlite-wal"
rm -f "${PROFILE_DIR}/storage-sync-v2.sqlite-wal"
rm -f "${PROFILE_DIR}/webappsstore.sqlite"
rm -f "${PROFILE_DIR}/xulstore.json"


LIMIT=$(nproc)

parallel() {
    while IFS='' read -r LINE ; do
        # parallel thread seem overkill on a daily basis. maybe when first run on a profile?
        while [ "$(track_pids)" -ge "${LIMIT}" ] ; do
            printf 'All procs busy yet.\n' >&2
            sleep 1
        done
        printf "Cleansing %s\n" "${LINE}" >&2
        sqlite3 "${LINE}" vacuum && sqlite3 "${LINE}" reindex &
        track_pids $!
    done
}

profile_cleaner() {
    printf "Cleansing start.\n"
    # mimetype verification part is necessary as mozilla also uses Berkeley DB...
    # GraySky profile cleaner is fliexible, but non-posix, not packaged and PID hungry.
    cd "${PROFILE_DIR}" || die "could not cd to '${PROFILE_DIR}'"
    find ./ \( ! -path './*/*' -o -prune \) -type f \
        | tail -n +1 \
        | grep -E '.*\.sqlite(-shm)?(.bak)?$|.*\.db$' \
        | LC_MESSAGES=POSIX xargs file -i \
        | sed -n 's/^\(.*\):\s*application\/vnd\.sqlite3.*$/\1/p' \
        | parallel
    wait
    printf "Cleansing done.\n"
}

profile_cleaner
while [ "$(track_pids)" -ne 0 ] ; do
    printf 'Waiting for sqlite3 background instanceto finish.\n' >&2
    sleep .5
done

size_bytes=$(( $(du -sk "${FF_in_RAM}" | awk '{print $1}') * 1000 ))
# todo fix : no error handling down here... and no duplicated output with no progress in terminal
# https://www.gnu.org/software/tar/manual/html_section/verbose.html
# we could check pv is installed and use it... saving the background loop.
# we'd still need 'du' to calculate, and pipe would still make error handling touchy.
# '> >()' trick is not posix. https://stackoverflow.com/a/12041373
tar -C "${FF_in_RAM}" -cpzf "${FF_in_RAM}/${arch_name}" "firefox/" --totals=SIGUSR1 2>&1 \
    | unbuffered_sed "${size_bytes}" \
    | bc \
    | progress_notif_lines "Archiving profile…" &
PID="$(
    env LC_MESSAGES=POSIX ps -o pid=,ppid=,cmd= \
    | grep -e "${$} *tar" \
    | sed "s/${$}.*//g" \
    | tr -d ' '
)"
kill_usr1_loop "${PID}"

# [ -s "${FF_in_RAM}/${arch_name}" ] would only check size is not zero
if [ "$( du -k "${FF_in_RAM}/${arch_name}" | cut -f1 )" -lt 15000 ] ; then
    die "file '${FF_in_RAM}/${arch_name}' has unconsistent size"
fi

mkdir "${arch_path%/*}" >/dev/null 2>&1
mv "${FF_in_RAM}/${arch_name}" "${arch_path}"
progress_notification "Profile archived." 100
