#!/bin/sh

# there exist no compositor-agnostic solution yet
#   (search foreign_toplevel_manager dropdown)
# there is foreign-toplevel, from wlroots' examples
#   https://www.reddit.com/r/swaywm/comments/9yv698/dropdown_terminal_via_scratchpad/
# there is a hack for sway
#   https://www.reddit.com/r/swaywm/comments/9yv698/dropdown_terminal_via_scratchpad/

die() { printf "%s\n" "$1" >&2 ; exit 1 ; }

launch() {
    nohup "$@" </dev/null >/dev/null 2>&1 &
    # shell limitation: can't replace last newline with ";"
}

if [ "$#" -gt 0 ] && [ ! -d "$1" ] ; then
    die "Argument must be a folder."
fi    

# launch terminal as a fallback
if ! command -v foreign-toplevel >/dev/null 2>&1 ; then
    # not hardcoded, can't use -c "${1:-${HOME}}"
    launch "${COLOREXE}"
    die "foreign-toplevel not found."
fi

DROPDOWN_ID="dd-terminal"

get_wid () { foreign-toplevel | grep "${DROPDOWN_ID}" | tr '.' ' ' | cut -f2 -d' ' ; }
is_minimized () { foreign-toplevel | grep "${DROPDOWN_ID}" | grep " minimized" || false ; }
is_focused () { foreign-toplevel | grep "${DROPDOWN_ID}" | grep " active" || false ; }

if [ -z "$(get_wid)" ]; then
    if command -v foot >/dev/null 2>&1 ; then
        if command -v tmux >/dev/null 2>&1 ; then
            if [ "$(pgrep -fc tmux)" -gt 0 ] ; then
                printf "tmux already running.\n" >&2
            fi
            launch foot \
                --window-size-pixels="1628x650" \
                --app-id="${DROPDOWN_ID}" \
                tmux
        else
            launch foot \
                --window-size-pixels="1628x650" \
                --app-id="${DROPDOWN_ID}"
        fi
    else
        die "Install foot."
    fi
    sleep 0.2
    exit
fi

if is_minimized ; then
    foreign-toplevel -r "$(get_wid)" # unminimize
    foreign-toplevel -f "$(get_wid)" # focus
else
    if is_focused ; then
        foreign-toplevel -i "$(get_wid)" # minimize
    else
        foreign-toplevel -f "$(get_wid)" # focus
    fi
fi

