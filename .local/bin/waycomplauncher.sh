#!/bin/sh
# todo: read https://github.com/Vladimir-csp/uwsm/blob/master/wayland-session
# https://gitlab.freedesktop.org/wlroots/wlroots/-/blob/master/docs/env_vars.md

die () { printf "%s\n" "${1}" >&2; exit 1; }
command -v "${1}" >/dev/null 2>&1 || die "usage: ${0##*/} <compositor>"

# set in .env.sh
#export XDG_CONFIG_HOME=$HOME/.config

# for firefox to find icons
export XDG_DATA_DIRS=/usr/share:/usr/local/share:~/.local/share
# not setting XDG_CACHE_HOME, XDG_DATA_HOME, XDG_STATE_HOME explicitly (yet).
# logind sets XDG_RUNTIME_DIR (to /run/user/UID), this is for non-systemd machines
if [ -z "${XDG_RUNTIME_DIR}" ] ; then
    XDG_RUNTIME_DIR=/tmp/session-"$(id -u)"
    export XDG_RUNTIME_DIR
    if [ ! -d "${XDG_RUNTIME_DIR}" ] ; then
        mkdir -p "${XDG_RUNTIME_DIR}" || die "XDG_RUNTIME_DIR creation failed."
        chmod 0700 "${XDG_RUNTIME_DIR}" || die "XDG_RUNTIME_DIR chmod failed."
    fi
fi

if [ ! -e ~/.WP ] ; then
    wget -q -O ~/.WP \
        https://upload.wikimedia.org/wikipedia/commons/archive/d/db/20181230091826%21Pygoscelis_papua_-Nagasaki_Penguin_Aquarium_-swimming_underwater-8a.jpg
fi


# https://github.com/swaywm/sway/wiki/Running-programs-natively-under-wayland

export LIBSEAT_BACKEND=logind       # logind|seatd|builtin
export XDG_SESSION_TYPE=wayland     # undo with 'unset XDG_SESSION_TYPE'
export XDG_CURRENT_DESKTOP=sway     # for waybar tray to work
# WAYLAND_DISPLAY (used by mate-panel) is set by sway

# for containers, install a xdg-desktop-portal backend, like xdg-desktop-portal-wlr

export XCURSOR_THEME=ComixCursors-Slim-White
# export XCURSOR_SIZE=24
# export MESA_LOADER_DRIVER_OVERRIDE=crocus
export SDL_VIDEODRIVER=wayland
export MOZ_ENABLE_WAYLAND=1         # firefox
export MOZ_DBUS_REMOTE=1            # https://mastransky.wordpress.com/2020/03/16/wayland-x11-how-to-run-firefox-in-mixed-environment/
export LITE_SCALE=1                 # lite-xl
# ECORE_EVAS_ENGINE =wayland_shm    # or "wayland_egl"
# ELM_DISPLAY = "wl"

# export OOO_FORCE_DESKTOP=gnome
# export SAL_USE_VCLPLUGIN=gtk

##### Gtk #####
export GDK_BACKEND=wayland          # not recommended by sway in link above, but necessary for copy-paste in firefox)
# export GTK_USE_PORTAL=0           # https://github.com/swaywm/sway/wiki#gtk-applications-take-20-seconds-to-start
# export CLUTTER_BACKEND=wayland
export CLUTTER_BACKEND=gdk          # https://gitlab.gnome.org/GNOME/clutter/-/merge_requests/5
export NO_AT_BRIDGE=1               # so gtk3 apps don't launch a stupid org.a11y.atspi.Registry deamon

##### Qt #####
# export QT_QPA_PLATFORM=wayland      # wayland|wayland-egl|eglfs
# export QT_QPA_PLATFORMTHEME=qt6ct
# export QT_WAYLAND_DISABLE_WINDOWDECORATION=1 # don't draw client-side decorations
# export QT_WAYLAND_SHELL_INTEGRATION=xdg-shell-v6
# export QT_GRAPHICSSYSTEM=raster
# export QT_DEVICE_PIXEL_RATIO=72   # deprecated
# export QT_QPA_EGLFS_PHYSICAL_WIDTH=1920
# export QT_QPA_EGLFS_PHYSICAL_HEIGHT=1080
export QT_AUTO_SCREEN_SCALE_FACTOR=0  # or 1 (scribus, mkvtoolnix...)
# export QT_SCREEN_SCALE_FACTORS=1        # 1;2;1 for multiple screens
# export QT_SCALE_FACTOR=1                    # this is application-wise ( != screen-wise )
# export QT_WAYLAND_FORCE_DPI=96
# export QT_WAYLAND_FORCE_DPI=physical

#localectl list-x11-keymap-variants fr
export XKB_DEFAULT_MODEL=pc105
export XKB_DEFAULT_LAYOUT=fr
export XKB_DEFAULT_VARIANT=oss
export XKB_DEFAULT_OPTIONS=terminate:ctrl_alt_bksp
# XKB_DEFAULT_RULES

# https://wiki.archlinux.org/title/Input_method
# export GTK_IM_MOUDLE=xim
# export XMODIFIERS=@im=ibus
# export QT_IM_MODULE=ibus

# WINEDLLOVERRIDES=winemenubuilder.exe=d

# dbus-launch is not availlable on fresh alpine install. ddevault uses dbus-run-session
# https://git.sr.ht/~sircmpwn/dotfiles/tree/master/item/bin/sway-run
exec dbus-run-session -- "${1}" -d >"/tmp/${1##*/}-$(id -u).log" 2>&1
