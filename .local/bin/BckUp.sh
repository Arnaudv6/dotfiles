#!/bin/sh
# copyright: Arnaud Vallette d'Osia - 2022
# license: https://www.gnu.org/licenses/gpl-3.0.txt
# this file is a part of my dotfiles

set -o nounset  # dev phase: exit if met with uninitialized variable

# script auto-run options:
# a udev rule
#   + could run this script in a terminal when HDD is plugged
#   - doesn't serve as a reminder to actually plug the disk
#   - automates nothing but one 1 script run per HDD plugging
#   - which userid for script execution?
# running at boot 'if ! yes_NO "Run BackUp?"; then exit ; fi'
#   + warn user if last backup dates ( from kernel .config for instance )


# todo:
# the meta-game is mounting src as well as dst
#  backup SRC header as well as DST headers. checksum them?
#  allowing specific folder in partition
#  use current mountpoint if already mounted
#  I think it is safe asumption to say a mounted disk is clean, or not free to unmount for fs.chk
#  ensure there is enough free space
#  don't unmount parts that were mounted beforehand (on either crash or normal exit)
#  invoke/revoke or ready/unready disk, (stay: don't unmount if was not mounted...)
#  is_recent_file() => execute if old (36ks = 10h)
#  state hypotheses: src must be mounted? powered? Partition-level LUKS support.
#  exclude verbose folders: blender, .config/libreoffice
#  tar.gz emails?

# https://superuser.com/a/1834490/1899645
clear_stdin() {
    old_tty_settings="$(stty -g)"
    stty -icanon min 0 time 0
    # while read -r _; do :; done
    # while dd ibs=1 count=1 ; do :; done
    cat - > /dev/null
    stty "$old_tty_settings"
    unset old_tty_settings
}

# Cursor related code
# (Saved) cursor positions are absolute: terminal rows and cols -> wrong by an offset as
# soon as terminal scrolls. So at script launch, we first ensure headroom,
# then we always check we did not reach screen end before restoring a cursor position.
    printf '\e[2J\e[H' # supposedly sometimes nicer on scrollback history than 'clear'
    TERM_HEIGHT="$(stty size | cut -d ' ' -f1)"
    export TERM_HEIGHT
    CURSOR_LIFO_STRING= # push/pop from string's START! (to avoid 'rev | cut | rev')
    export CURSOR_LIFO_STRING

    # Hard-won solution. Before editing, read ALL of https://stackoverflow.com/a/2575525
    # based on https://vt100.net/docs/vt510-rm/CPR.html
    get_cursor_row() {
        oldstty=$(stty -g)
        stty -icanon -echo min 1 time 0
        printf '\033[6n' > /dev/tty
        # 'cut' since this script only ever needs row information (not column)
        pos=$(dd count=1 2> /dev/null | tr -dc '[:digit:];' | cut -d ';' -f 1 )
        stty "${oldstty}"
        printf "%d" "${pos}"
    }

    push_cursor() { CURSOR_LIFO_STRING="$(get_cursor_row) ${CURSOR_LIFO_STRING}" ; }

    pop_cursor() {
        # for dev phase: printf "we 'll move cursor up\n" ; sleep 2
        # '\033[<LINE>;<COLUMN>H' moves cursor to absolute position.
        # '\e[J' deletes to end from one line bellow.
        pos=$( printf "%s" "${CURSOR_LIFO_STRING}" | cut -d ' ' -f 1)
        CURSOR_LIFO_STRING=$( printf "%s" "${CURSOR_LIFO_STRING}" | cut -d ' ' -f 2-)
        if [ "$(get_cursor_row)" != "${TERM_HEIGHT}" ] ; then
            printf '\e[%s;0H\e[J' "${pos}"
        fi
    }

    forget_cursor() {
        CURSOR_LIFO_STRING=$( printf "%s" "${CURSOR_LIFO_STRING}" | cut -d ' ' -f 2-)
    }

crypt_part_dev() {
    # if disk is connected, return partition device
    test -n "${DST_PART_UUID}" \
    && test -e /dev/disk/by-uuid/"${DST_PART_UUID}" \
    && printf "/dev/disk/by-uuid/%s" "${DST_PART_UUID}"
}

part_dev(){
    crypt_part_dev || (
        test -n "${DST_FSYS_UUID}" \
        && test -e /dev/disk/by-uuid/"${DST_FSYS_UUID}" \
        && printf "/dev/disk/by-uuid/%s" "${DST_FSYS_UUID}"
    )
}

is_mounted() {
    maybe="$(lsblk -no MOUNTPOINTS "$(fs_or_mapper_path)" 2> /dev/null)"
    # maybe="$(lsblk -no MOUNTPOINT "$(fs_or_mapper_path)" 2> /dev/null)"
    # MOUNTPOINTS with a "S": v2.37-ReleaseNotes
    # https://git.kernel.org/pub/scm/utils/util-linux/util-linux.git/commit/misc-utils/lsblk.h?id=c6648d2db1716978c8344c046ea6c94a68d27238
    if [ -n "$maybe" ] ; then
        printf "%s" "$maybe"
        unset maybe
    else
        unset maybe
        false
    fi
}

fs_or_mapper_path() {
    if [ -z "${DST_PART_UUID}" ] ; then
        printf '/dev/disk/by-uuid/%s' "${DST_FSYS_UUID}"
    else
        lsblk -no PATH "$(crypt_part_dev)" | grep /dev/mapper/
    fi
}

log_OK() { printf '\e[32m [ OK ]\e[0m %b\n' "${1}"; }
log_XX() { printf '\n\e[31m [ %s ]\e[0m %b\n' "${1}" "${2}" >&2 ; }
# suggested usage: log_XX [DD|WW|EE|!!] [message]
# do we want a more complex logger, with a case statement?
# OK green    \033[32m
# DD blanc    \033[39m
# ww orange   \033[33m
# ee red      \033[31m
# !! red      \033[31m

breakpoint() {
    # SIGINT trap will catch ^C, from 'read', to exit loops, from any term mode.
    printf "[Enter] to continue with script execution or [ctrl]+[c] to quit\n"
    read -r _
}

yes_NO() {
    # TODO: fix Ctrl+C does not interupt.
    printf '\e[33m [USER]\e[0m %s (y/N) ' "${1} "

    old_tty_settings=$(stty -g)
    stty -icanon -echo min 1 time 0
    answer=$(dd count=1 2> /dev/null)
    stty "$old_tty_settings"
    unset old_tty_settings

    if printf "%s" "${answer}" | grep -qi '^Y' ; then
        printf 'yes.\n'
        return
    fi
    printf 'no.\n'
    false
}

# save terminal state to restore on exit. 'stty' is posix. 'reset' is not.
SAVED_TTY="$(stty -g)"
export SAVED_TTY
die() {
    trap - EXIT HUP QUIT TERM INT ABRT
    stty "${SAVED_TTY}"
    clear_stdin

    # POSIX man: die() will not receive information about which signal was caught.
    log_XX '!!' "${1:-Interrupted.}"

    # unmount_part() & lock() can take forever (loop over a user prompt) try anyway.
    [ -n "${DST_FSYS_UUID:-}" ] && unmount_part
    [ -n "${DST_PART_UUID:-}" ] && lock
    exit 1
}
trap 'die' EXIT HUP QUIT TERM INT ABRT

is_consistent_file_or_die() {
    if [ -z "$1" ] || [ ! -r "$1" ] || [ "$( du -k "${1}" | cut -f1 )" -lt 4 ] ; then
        die "file '${1}' does not exist or has unconsistent size"
    fi
    # Parsing 'ls' ouput is fragile, even more so when searching (localized) dates.
    # '--full-time' is not posix. Yet this is tested with busybox and coreutils.
    # https://unix.stackexchange.com/questions/561927
    # shellcheck disable=SC2010
    if ! ls --full-time "${1}" | grep -q "$(date '+%Y-%m-%d')" ; then
        die "file '${1}' was not created today"
    fi
}

admin() {
    # memoize the privileges elevation command
    if [ -z "${RUN_AS_ADMIN:-}" ] ; then
        if command -v doas >/dev/null 2>&1 ; then
            export RUN_AS_ADMIN=doas
        elif command -v sudo >/dev/null 2>&1 ; then
            export RUN_AS_ADMIN=sudo
        else
            die "Install doas or sudo"
        fi
    fi

    # loop around (doas) if failed authentication
    admin_attempts=0  # beware names collision, as we unset this var.
    while ! ${RUN_AS_ADMIN} "$@" ; do
        admin_attempts=$((admin_attempts+1))
        if [ $((admin_attempts%3)) = 0 ] ; then breakpoint ; fi
    done
    unset admin_attempts
}

check_part_fs() {
    push_cursor
    printf "Running fs.chk\n"
    # adapt to your FS type
    # || die only makes sure we don't mess 3 passwords and silently continue
    admin fsck.ext4 "$(fs_or_mapper_path)" || die "error while fsck."
    breakpoint
    pop_cursor
    log_OK "fs.chk"
}

unlock() {
    push_cursor
    printf "unlocking FS\n"
    # udisksctl instead of looping around cryptsetup (mapper path is retreived anyway)
    # admin cryptsetup open --type luks "UUID=${DST_PART_UUID}" "${UNLOCKED_PART##*/}"
    udisksctl unlock --block-device "$(crypt_part_dev)"
    # todo actually this in unspecific: wrong passphrase will continue with mounting
    pop_cursor
    log_OK "partition unlocked"
}

lock() {
    push_cursor
    printf "locking FS\n"
    udisksctl lock --block-device "$(crypt_part_dev)"
    pop_cursor
    log_OK "partition locked"
}

mount_part() {
    push_cursor
    printf "mounting FS\n"
    # udisksctl instead of looping around 'admin mount': no need do ensure
    # mountpoint dir exists beforehands, and mountpoint is retreived anyway
    # but we must get /dev/sdXN path until this is fixed:
    # https://gitlab.alpinelinux.org/alpine/aports/-/issues/13501
    # https://lord.re/posts/175-automount-sur-alpine/
    # possible cause: alpine does not create /dev/disk/by-uuid symlink to mapper devices.
    if [ -z "${DST_PART_UUID}" ] ; then
        # don't run as root, so unpriviledged have access.
        udisksctl mount -b "$(fs_or_mapper_path)"
    else
        # todo delete /media/bckUp folder when this workaround is no longer needed.
        if [ ! -d /media/bckUp/ ] ; then
            doas mkdir /media/bckUp/
        fi
        admin mount "$(fs_or_mapper_path)" /media/bckUp/
    fi
    pop_cursor
    log_OK "mounted FS"
}

unmount_part() {
    push_cursor
    printf "unmounting FS\n"
    if [ -z "${DST_PART_UUID}" ] ; then
        udisksctl unmount -b "$(fs_or_mapper_path)"
    else
        admin umount "$(fs_or_mapper_path)"
    fi
    pop_cursor
    log_OK "unmounted FS"
}

backup_luks_header() {
    push_cursor
    printf "backing luks header\n"
    # cryptsetup luksHeaderBackup can't overwrite
    header_filename=${DST_PART_UUID%%-*}-luks.header
    tmp=/tmp/${header_filename}
    [ -f "${tmp}" ] && admin rm "${tmp}"
    # double dash '--' not needed on recent opendoas/sudo versions.
    admin cryptsetup luksHeaderBackup "$(crypt_part_dev)" --header-backup-file "${tmp}"

    printf "changing ownership of luks header to %s\n" "${USER}"
    admin chown "${USER}:${USER}" "${tmp}"
    # $USER is expanded before elevation
    is_consistent_file_or_die "${tmp}"
    mv -f "${tmp}" "${bckup_dir}"
    pop_cursor
    log_OK "backed luks headers"
}

backup_etc_files() {
    push_cursor
    printf "Backing /etc files\n"
    # todo: non-silently exclude sensible files?
    admin tar -Pczf "${bckup_dir}/etcFiles.tar.gz" /etc
    # # can't both protect it from unpriviledge reads and save it with unpriviledged rsync
    # etc_archive="${bckup_dir}/etcFiles.tar.gz"
    # # sub-shell ensures file is owned by root.
    # admin sh -c "tar -PczO /etc > '${etc_archive}'"
    # # unpriviledged users should not have read access. (security)
    # admin chmod 600 "${etc_archive}"
    pop_cursor
    is_consistent_file_or_die "${bckup_dir}/etcFiles.tar.gz"
    log_OK "backed /etc/ files"
    unset etc_archive
}

backup_extlinux_conf() {
    extlinux_conf=/boot/extlinux.conf
    if [ -e "${extlinux_conf}" ] ; then
        cp "${extlinux_conf}" "${bckup_dir}/"
        is_consistent_file_or_die "${bckup_dir}/extlinux.conf"
        log_OK "backed extlinux.conf"
    else
        log_XX WW "no 'extlinux.conf' file"
    fi
    unset extlinux_conf
}

search_empty() {
    # todo: loop around?
    push_cursor
    if yes_NO "Search for empty files and directories?"; then
        find "${SRC_DIRECTORY}" -depth -empty \
            -not -path "${HOME}/Arnaud/Soft/*" \
            -not -path '*/\.*' 2>&1 | less -MI  # ignore hidden files
        breakpoint
        pop_cursor
        log_OK "listed empty files and dirs"
    else
        pop_cursor
    fi
}

search_broken_symlinks() {
    # todo: loop around?
    push_cursor
    if yes_NO "Search for broken symlinks?"; then
        find "${SRC_DIRECTORY}" -type l ! -exec test -e {} \; -print 2>&1 | less -MI
        breakpoint
        pop_cursor
        log_OK "listed broken symlinks"
    else
        pop_cursor
    fi
}

# For backup maintenance, walking complete drive (sudo cat /dev/sdXN > /dev/null)
# will automatically mark and replace bad sectors. No idea if this applies to SSDs.
# iotop reports 140Mo/s and to be exhaustive, we should walk both src and dst disks.
# Better use rsync checksum so: partitions table are yet read at part' mounting,
# cksum require computing power, but covers both src and dst (used sectors only).

# First non-checksum backup then checksum diff preview: help user be attentive.
# Don't auto act on it: checksum indicates a disk error on either SRC ot DST.
# No point saving list between runs to prevent rescan. https://serverfault.com/a/863248

run_backup() {
    printf "\e[32m [INFO]\e[0m %b backup started\n" "${SRC_DIRECTORY##*/}"

    if [ -n "${DST_PART_UUID}" ] ; then
        backup_luks_header
        unlock
    fi

    if ! is_mounted > /dev/null ; then
        check_part_fs
        mount_part
    fi

    DST_MOUNT_DIR="$(is_mounted)" || die "empty DST_MOUNT_DIR"

    if [ ! -w "${DST_MOUNT_DIR}" ] ; then
        user="$(id -u)"  || die "error retrieving user"
        group="$(id -u)"  || die "error retrieving group"
        admin chown "${user}:${group}" "${DST_MOUNT_DIR}"
        unset user group
    fi

    if [ -d "${DST_MOUNT_DIR}/lost+found" ] ; then
        admin rmdir "${DST_MOUNT_DIR}/lost+found" \
            || die "'${DST_MOUNT_DIR}/lost+found' is not empty."
    fi

    search_empty
    search_broken_symlinks
    push_cursor

    # depending on config, consider adding ".bash_history", ".mozilla", "./lost+found/"
    # todo: remove or use $SRC_BLACKLIST var (unused as it is)
    # todo: add $DESTDIR var, to sync /home/$USER to $DESTDIR and not sandbox user's docs
    opt='-aH --delete --exclude=.cache --exclude=.rtorrent --delete-excluded --exclude=bacasable'
    esc=$(printf '\033')

    while true ; do
        # todo: see POSIX exceptions here: https://www.shellcheck.net/wiki/SC2086
        rsync ${opt} --progress -n "${SRC_DIRECTORY}" "${DST_MOUNT_DIR}" \
            | sed "s,^deleting,${esc}[31m&${esc}[0m," 2>&1 | less -MIR  # requires GNU less.
        yes_NO "Preview actions once more?" || break
    done
    unset esc

    # --files-from=FILE
    if yes_NO "Perform previewed rsync?"; then
        if rsync ${opt} --info=progress2 "${SRC_DIRECTORY}" "${DST_MOUNT_DIR}" ; then
            pop_cursor
            log_OK "rSync runned effectively"
        else
            if ! yes_NO "Keep error displayed by not popping cursor?"; then
                pop_cursor
            else
                forget_cursor
            fi
            log_XX "EE" "rSync encountered an issue"
        fi
    else
        pop_cursor
        log_XX "WW" "Not running the backup."
    fi

    if yes_NO "Check files with checksum? (last run: ${CHECKSUM_DATE})"; then
        rsync ${opt} --checksum --progress -n "${SRC_DIRECTORY}" "${DST_MOUNT_DIR}"
        # -n = --dry-run
        printf "\n\nRemember to paste date in script:\n%s\n\n" "$(date -u)"
    fi

    unmount_part
    check_part_fs
    if [ -n "${DST_PART_UUID}" ] ; then lock ; fi

    if yes_NO "poweroff the disk?"; then shutdown_drive ; fi
}

shutdown_drive() {
    printf "%s\n" "$(part_dev)"
    disk_dev=$(realpath "$(part_dev)" | sed 's/[0-9]\+$//')

    if lsblk -no ROTA "$(part_dev)" | grep -q 1 ; then
        if command -v smartctl >/dev/null 2>&1 ; then
            push_cursor
            printf "running smartmontools diagnostic\n"
            # todo: test this fixes admin | less crappy stdin/stdout mix
            admin sh -c "smartctl --all '${disk_dev}' 2>&1 | less -MI"
            pop_cursor
            log_OK "runned smartmontools diagnostic"
        else
            log_XX "WW" "smartctl command not found, skipping smartmontools diagnostic."
        fi
    fi

    if command -v udisksctl >/dev/null 2>&1 ; then
        #  - umount (sufficient with USB flash drives)
        #  - hdparm -Y if this is a HDD : to spin down
        #  - eject if this is a CD
        udisksctl power-off -b "${disk_dev}"
        log_OK "backup disk power-off"
    else
        log_XX "WW" "udisks not installed, could not power-off the backup drive."
    fi

    unset disk_dev
    log_XX "DD" "don't forget to physically disconnect the backup drive."
}

check_dependency() {
    if ! command -v "$1" >/dev/null 2>&1 ; then
        die "'${1}' not in path: missing package?"
    fi
}

# todo update dependencies list
check_dependency readlink
check_dependency realpath
check_dependency fsck.ext4
check_dependency cryptsetup
check_dependency mount
check_dependency umount
check_dependency rsync
check_dependency lsblk # util-linux

# set terminal window (or tab) title
printf '\e]0;%b\a' "${0##*/}"

# print script name and basic instructions for the user
printf '\n########## %s ##########\n' "${0##*/}"
printf 'Note: \e[31mnever\e[0m is root password asked, '
printf 'only encryption keys or \e[32muser\e[0m password for elevation.\n\n'

# first backup important system files in ${HOME}
export bckup_dir=~/Sauvegardes/Alpine
mkdir -p "${bckup_dir}"

if command -v apt-mark >/dev/null 2>&1 ; then
    apt-mark showmanual > "${bckup_dir}/world"
elif command -v emerge >/dev/null 2>&1 ; then
    cp /var/lib/portage/world "${bckup_dir}/"
elif command -v apk >/dev/null 2>&1 ; then
    cp /etc/apk/world "${bckup_dir}/"
fi
is_consistent_file_or_die "${bckup_dir}/world"
log_OK "backed world file"

# tar -Pczf "${bckup_dir}/myOverlay.tar.gz" /var/lib/arnaud/
# is_consistent_file_or_die "${bckup_dir}/myOverlay.tar.gz"
# log_OK "backed my ebuilds"

# todo: fix for alpine kernel
# cp /usr/src/linux/.config "${bckup_dir}/kernel.config"
# is_consistent_file_or_die "${bckup_dir}/kernel.config"
# log_OK "backed kernel .config"

backup_etc_files

backup_extlinux_conf

# backup gadgets: tablets, raspberryPis, router...
if yes_NO "Run sync_devices script?"; then
    # todo propose user that we push and pop cursors around?
    # archive this script and put the 2 useful lines here?
    sync_devices
    clear_stdin
fi


# don't parse fstab, /proc/mount or /sys/block/
# util-linux package: blkid (admin rights required), lsblk, findmnt, mountpoint
#   (doas) lsblk -o NAME,UUID,PTUUID,PARTUUID,PATH,RM,ROTA,STATE,MOUNTPOINTS,TYPE
# coreutils (or busybox) package: readlink, realpath
# udisks2 (or udiskie) package: udisksctl

# use lsblk to find UUIDs upfront.
# Don't try to parse fstab/crypttab:
#  - it's fragile and generally a bad idea
#  - backup copies on multiple hard drives will make for complex fstab
#  - its hard to go from filesystem to associated mapper anyway
# trick to prevent udisks automouter daemon: add such a line in fstab
# UUID=<UUID> /media/none ext4 noauto,rw 0 0

# shellcheck disable=SC2317
home_to_on_site_HDD(){
    export SRC_DIRECTORY="/home/" # trailing slash
    export SRC_BLACKLIST="${bckup_dir}/blcklst"
    export DST_PART_UUID="5aba5353-b190-420e-8a45-66e56b90dc97"
    export DST_FSYS_UUID="7e708cf7-4980-456e-b73e-a4ae1bfe16c4"
    export CHECKSUM_DATE="2024/08/26 home only"
}

# shellcheck disable=SC2317
media_to_on_site_HDD(){
    export SRC_DIRECTORY="/media/media/" # trailing slash
    export SRC_BLACKLIST="/dev/null"
    export DST_PART_UUID=
    export DST_FSYS_UUID="069ef2d5-4354-485f-afa3-b4263186be1a"
    export CHECKSUM_DATE="2023?"
}

# shellcheck disable=SC2317
home_to_off_site_SSD(){
    export SRC_DIRECTORY="/home/" # trailing slash
    export SRC_BLACKLIST="${bckup_dir}/blcklst"
    export DST_PART_UUID="5153e89a-9811-476e-839a-c606bfcc28f5"
    export DST_FSYS_UUID="94f065d8-d497-4f8c-b34d-431cccde9929"
    export CHECKSUM_DATE="never?"
}

# not used yet, films would be too much
# shellcheck disable=SC2317
# media_to_off_site_SSD(){
    # export SRC_DIRECTORY="/media/media/" # trailing slash
    # export SRC_BLACKLIST="/dev/null"
    # export DST_PART_UUID=
    # export DST_FSYS_UUID="cc0e2bbc-a4d2-44a6-9be7-57d2b6236070"
#}

# shellcheck disable=SC2317
home_to_off_site_HDD(){
    export SRC_DIRECTORY="/home/" # trailing slash
    export SRC_BLACKLIST="${bckup_dir}/blcklst"
    export DST_PART_UUID="f43d117d-d954-4560-b0f6-ef1e8c6db78e"
    export DST_FSYS_UUID="499480c8-9d63-44a8-b11e-6515bf8a2fc3"
    export CHECKSUM_DATE="never?"
}

# shellcheck disable=SC2317
parents_in_desktop_replication(){
    export SRC_DIRECTORY="/home/" # trailing slash
    export SRC_BLACKLIST="/dev/null"
    export DST_PART_UUID="6e22f547-a344-4126-81f8-1dc3ba989f81"
    export DST_FSYS_UUID="33fc93d4-b8ed-416e-aad7-930584e50e0f"
    export CHECKSUM_DATE="never?"
}

for bck_job in \
        home_to_on_site_HDD \
        media_to_on_site_HDD \
        home_to_off_site_SSD \
        home_to_off_site_HDD \
        parents_in_desktop_replication ; do
    eval "${bck_job}"
    if [ -n "$(part_dev)" ] ; then
        if yes_NO "run ${bck_job}?"; then
            run_backup
        fi
    fi
done

if [ -n "${CURSOR_LIFO_STRING}" ] ; then
    log_XX "WW" "Cursor positions FIFO stack sanity check failed."
fi

trap - EXIT
exit
