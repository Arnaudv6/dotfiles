#!/bin/sh

# .profile is only ever sourced for login shells, never after (interractive or not)
# no need for 'if ! echo $- | grep -q "i" ; then return; fi'

if [ -n "$BASH_VERSION" ]; then
    [ -f ~/.bashrc ] && echo 'consider removing ~/.bashrc'
    export POSIXLY_CORRECT=y  # see .README.mk
fi

# shellcheck disable=SC1090
. ~/.env.sh
export ENV="${HOME}/.aliases.sh"  # see .README.mk about the naming

# Auto start compositor on first VT
# don't forget to change default symlink: 'systemctl set-default multi-user.target'
# Because, as shown with 'less /usr/lib64/systemd/system/graphical.target'
# "graphical.target" = "multi-user.target" + "display-manager.service"
if command -v labwc >/dev/null 2>&1 && [ "$(tty)" = "/dev/tty1" ] ; then
    printf "Press any key to prevent launching wayland compositor\n"
    old_tty_settings=$(stty -g)
    stty -icanon -echo min 0 time 15  # tenths of a second. yep.
    answer=$(dd count=1 2> /dev/null)
    stty "$old_tty_settings"
    unset old_tty_settings

    if [ -z "${answer}" ] ; then
        unset answer
        exec waycomplauncher.sh labwc
        # logout
    fi
    unset answer
fi

